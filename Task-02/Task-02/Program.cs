﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_02
{
    class Program
    {
        static void Main(string[] args)
        {
            var day = 1;
            var month = 1;
            var year = new string[12] { "January", "Feburary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
            
            Console.WriteLine("Please input the day of your birthday (1-31)");
            day = int.Parse(Console.ReadLine());
            Console.WriteLine("Please input the month of your birthday (1-12)");
            month = int.Parse(Console.ReadLine());
            Console.WriteLine($"Your birthday is on the {day} of {year[month - 1]}");


        }
    }
}
