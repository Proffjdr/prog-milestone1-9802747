﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task37
{
    class Program
    {
        static void Main(string[] args)
        {
            const int hpc = 10; // hours per credit
            const int ch = 5; //contact hours
            const int sem = 12; // semester length in weeks
            const int credits = 15; //credits per paper

            double output;

            output = ((hpc * credits) - (sem * ch)) / sem;
            Console.WriteLine($"The number of hours per week you need to study for a {credits} credit course is {output}");


        }
    }
}
