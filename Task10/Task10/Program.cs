﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task10
{
    class Program
    {
        static void Main(string[] args)
        {
            var year = 2016;
            var leapcount = 0;
            var i = 0;

            Start:
            Console.WriteLine("What is the current year?");
            if (int.TryParse(Console.ReadLine(), out year))
            {
                Console.WriteLine($"I will check the next 20 years from {year} and advise if they are leap years.");
                for (i = 0; i < 20; i++)
                {
                    if (year % 4 == 0)
                    {
                        if (year % 100 != 0 || year % 400 == 0)
                        {
                            leapcount++;
                        }
                    }
                    year++;


                }

                Console.WriteLine($"There are {leapcount} leap years in the 20 years following the year entered.");

            }

            else
            {
                Console.WriteLine("Please input a valid year");
                goto Start;
            }
        }
    }
}
