﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task30
{
    class Program
    {
        static void Main(string[] args)
        {
            string input1;
            string input2;
            int input1num;
            int input2num;

            Input1:
            Console.WriteLine("Please input a number");
            Console.Write(":");
            input1 = Console.ReadLine();
            if (int.TryParse(input1, out input1num)){
                goto Input2;
            }
            else
            {
                Console.WriteLine("Please input a valid number");
                goto Input1;
            }
            
            
            Input2:
            Console.WriteLine("Please input a second number");
            Console.Write(":");
            input2 = Console.ReadLine();
            if (int.TryParse(input2, out input2num))
            {
                goto Output;
            }
            else
            {
                Console.WriteLine("Please input a valid number");
                goto Input2;
            }
            
            Output:
            Console.WriteLine("As a string:");
            Console.WriteLine(input1 + input2);
            Console.WriteLine("");
            Console.WriteLine( "As a number:");
            Console.WriteLine(input1num + input2num);

        }
    }
}
