﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task21
{
    class Program
    {
        static void Main(string[] args)
        {
            var month = new Dictionary<string, int>();
            month.Add("January", 31);
            month.Add("February", 28);
            month.Add("March", 31);
            month.Add("April", 30);
            month.Add("May", 31);
            month.Add("June", 30);
            month.Add("July", 31);
            month.Add("August", 31);
            month.Add("September", 30);
            month.Add("October", 31);
            month.Add("November", 30);
            month.Add("December", 31);

            foreach(var x in month)
            {
                if (x.Value > 30)
                {
                    Console.WriteLine($"{x.Key} has 31 days!");
                }

            }
     
        }
    }
}
