﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task04
{
    class Program
    {
        static void Main(string[] args)
        {
            var select = 0;
            var input = 0.0;
            
            Menu:
            Console.WriteLine("**********************");
            Console.WriteLine("[1] to convert C to F");
            Console.WriteLine("[2] to convert F to C");
            Console.WriteLine("[3] Exit");
            Console.WriteLine("**********************");
            int.TryParse(Console.ReadLine(), out select);
            switch (select)
            {
                case 1:
                    Console.WriteLine("How many C would you like to convert to F?");
                    if (double.TryParse(Console.ReadLine(), out input))
                    {
                        Console.WriteLine($"{input}C is equal to {(input * 9) / 5 + 32}F");
                        Console.WriteLine("Press any key to continue...");
                        Console.ReadKey(true);
                        goto Menu;
                        
                    }
                    else
                    {
                        Console.WriteLine("invalid entry");
                        goto Menu;
                    }

                    break;

                case 2:
                    Console.WriteLine("How many F would you like to convert to C?");
                    if (double.TryParse(Console.ReadLine(), out input))
                    {
                        Console.WriteLine($"{input}F is equal to {(input - 32) * 5 / 9}C");
                        Console.WriteLine("Press any key to continue...");
                        Console.ReadKey(true);
                        goto Menu;
                    }
                    else
                    {
                        Console.WriteLine("invalid entry");
                        goto Menu;
                    }
                    break;

                case 3:
                    Console.WriteLine("Have a Nice day!");
                    Environment.Exit(0);
                    break;
                    
                default:
                    Console.WriteLine("Invalid entry");
                    goto Menu;
                    break;
            }
        }
    }
}
