﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task38
{
    class Program
    {
        static void Main(string[] args)
        {
            double input;
            
            Start:
            Console.WriteLine("Please input a number");
            Console.Write(":");
           
            if (double.TryParse(Console.ReadLine(), out input))
            {
                if (input == 0)
                {
                    Console.WriteLine("Cannot divide 0!");
                }
                else
                {
                    Console.WriteLine($"{input} / 1 is {input / 1}");
                    Console.WriteLine($"{input} / 2 is {input / 2}");
                    Console.WriteLine($"{input} / 3 is {input / 3}");
                    Console.WriteLine($"{input} / 4 is {input / 4}");
                    Console.WriteLine($"{input} / 5 is {input / 5}");
                    Console.WriteLine($"{input} / 6 is {input / 6}");
                    Console.WriteLine($"{input} / 7 is {input / 7}");
                    Console.WriteLine($"{input} / 8 is {input / 8}");
                    Console.WriteLine($"{input} / 9 is {input / 9}");
                    Console.WriteLine($"{input} / 10 is {input / 10}");
                    Console.WriteLine($"{input} / 11 is {input / 11}");
                    Console.WriteLine($"{input} / 12 is {input / 12}");
                    Environment.Exit(0);
                }
                Console.WriteLine("Please input a valid number");
                Environment.Exit(0);
            }
            else
            {
                Console.WriteLine("Please input a valid number");
                goto Start;

            }

        }
    }
}
