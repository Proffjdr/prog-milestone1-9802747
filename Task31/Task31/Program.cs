﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task31
{
    class Program
    {
        static void Main(string[] args)
        {
            string a;
            var input = 0;

            Start:
            Console.WriteLine("Please input a number");
            a = Console.ReadLine();
            if (int.TryParse(a, out input))
            {
                if (input % 3 == 0)
                {
                    Console.WriteLine($"This number is divisible by 3 to a whole number, the result is {input / 3}");
                    if (input % 4 == 0)
                    {
                        Console.WriteLine($"This number is divisible by 4 to a whole number, the result is {input / 4}");
                    }
                    else
                    {
                        Console.WriteLine("This number is not divisible by 4 to a whole number.");
                    }
                }
                else
                {
                    Console.WriteLine("This number is not divisible by 3 to a whole number.");
                    if (input % 4 == 0)
                    {
                        Console.WriteLine($"This number is divisible by 4 to a whole number, the result is {input / 4}");

                    }
                    else
                    {
                        Console.WriteLine("This number is not divisible by 4 to a whole number.");
                    }
                }

            }
            else
            {
                Console.WriteLine("Please input a valid number");
                goto Start;
                
            }

                
            


                    
        }
    }
}
