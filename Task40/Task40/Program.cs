﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task40
{
    class Program
    {
        static void Main(string[] args)
        {

            string input;

            Console.WriteLine("On a month that starts on a monday, assuming 31 days, there are 5 wednesdays. less and there are 4 wednesdays.");
            Console.WriteLine(":)");
            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            Console.WriteLine("Okay lets try again");
            Start:
            Console.WriteLine("Are there 31 days in the month? (y/n)");
            Console.Write(":");
            input = Console.ReadLine();
            if (input == "y" || input == "Y")
            {
                Console.WriteLine("On a month starting on a monday, There are 5 wednesdays in the month");
                Environment.Exit(0);

            }
            if (input == "n" || input == "N")
            {
                Console.WriteLine("On a month starting on a monday, There are 4 wednesdays in the month");
                Environment.Exit(0);
            }
            else
            {
                Console.WriteLine("Please input y (yes) or n (no)");
                goto Start;
            }
        }
    }
}
