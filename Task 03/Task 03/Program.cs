﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_03
{
    class Program
    {
        static void Main(string[] args)
        {
            const double convert = 0.62137;
            var select = 0;
            var input = 0.0;

        Menu:
            Console.WriteLine("*************************");
            Console.WriteLine("");
            Console.WriteLine("[1] Kilometres to Miles");
            Console.WriteLine("[2] Miles to Kilometres");
            Console.WriteLine("[3] Exit Application");
            Console.WriteLine("");
            Console.WriteLine("*************************");
            select = int.Parse(Console.ReadLine());
            switch (select)
            {
                case 1:
                    Console.WriteLine("How many KMs would you like to convert to miles?");
                    input = double.Parse(Console.ReadLine());
                    Console.WriteLine($"{input} kms is equal to {input * convert} miles");
                    Console.WriteLine("Press any key to continue...");
                    Console.ReadKey(true);
                    goto Menu;
                    break;
                    
                case 2:
                    Console.WriteLine("How many Miles would you like to convert to KMs?");
                    input = double.Parse(Console.ReadLine());
                    Console.WriteLine($"{input} miles is equal to {input / convert} KMs");
                    Console.WriteLine("Press any key to continue...");
                    Console.ReadKey(true);
                    goto Menu;
                    break;

                case 3:
                    Console.WriteLine("Have a nice day!");
                    Environment.Exit(0);
                    break;

                default:
                    Console.WriteLine("Invalid entry");
                    goto Menu;


            }
            
           
        }
    }
}
