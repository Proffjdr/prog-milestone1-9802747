﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task34
{
    class Program
    {
        static void Main(string[] args)
        {
            var weeks = 0;

            Console.WriteLine("Please input number of weeks");
            weeks = int.Parse(Console.ReadLine());
            Console.WriteLine($"{weeks} weeks has {weeks * 5} working days.");
        }
    }
}
