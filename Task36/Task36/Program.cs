﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task36
{
    class Program
    {
        static void Main(string[] args)
        {
            var count = 0;
            Console.WriteLine("please input a how many times you would like this loop to run");
            count = int.Parse(Console.ReadLine());
            for (var i = 0; i < count; i++)
            {
                if (i+1 < count)
                {
                    Console.WriteLine("Not there yet!");
                }
                else
                {
                    Console.WriteLine("We made it!");
                }

            }


        }
    }
}
