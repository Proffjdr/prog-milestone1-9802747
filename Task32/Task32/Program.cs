﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task32
{
    class Program
    {
        static void Main(string[] args)
        {
            var dow = new string[7] { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };

            for (var i = 0; i < 7; i++)
            {
                Console.WriteLine($"Day {i+1} is {dow[i]}");
            }
        }
    }
}
