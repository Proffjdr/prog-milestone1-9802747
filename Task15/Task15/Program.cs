﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task15
{
    class Program
    {
        static void Main(string[] args)
        {
            var numbers = new List<int> { };
            var result = 0;
            var input = 0;
            var i = 0;

            Input1:
            Console.WriteLine("Please select first number.");
            if(int.TryParse(Console.ReadLine(), out input))
            {
                numbers.Add(input);
            }
            else
            {
                Console.WriteLine("Invalid input.");
                goto Input1;
            }
            
            Input2:
            Console.WriteLine("Please select second number.");
            if (int.TryParse(Console.ReadLine(), out input))
            {
                numbers.Add(input);
            }
            else
            {
                Console.WriteLine("Invalid input.");
                goto Input2;

            }

            Input3:
            Console.WriteLine("Please select third number.");
            if (int.TryParse(Console.ReadLine(), out input))
            {
                numbers.Add(input);
            }
            else
            {
                Console.WriteLine("Invalid input.");
                goto Input3;

            }

            Input4:
            Console.WriteLine("Please select fourth number.");
            if (int.TryParse(Console.ReadLine(), out input))
            {
                numbers.Add(input);
            }
            else
            {
                Console.WriteLine("Invalid input.");
                goto Input4;

            }

            Input5:
            Console.WriteLine("Please select fifth number.");
            if (int.TryParse(Console.ReadLine(), out input))
            {
                numbers.Add(input);
            }
            else
            {
                Console.WriteLine("Invalid input.");
                goto Input5;

            }

            for (i = 0; i < numbers.Count; i++)
            {
                result = result + numbers[i];
            }

            Console.WriteLine($"the sum of {numbers[0]} + {numbers[1]} + {numbers[2]} + {numbers[3]} + {numbers[4]} is {result}");
        }
    }
}
