﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task13
{
    class Program
    {
        static void Main(string[] args)
        {
            double value1; ;
            double value2; ;
            double value3; ;
            double total;
                     

            Console.WriteLine("Please enter the first value");
            Console.Write("$");
            value1 = double.Parse(Console.ReadLine());

            Console.WriteLine("Please enter the second value");
            Console.Write("$");
            value2 = double.Parse(Console.ReadLine());

            Console.WriteLine("Please enter the third value");
            Console.Write("$");
            value3 = double.Parse(Console.ReadLine());

            total = (value1 + value2 + value3)*1.15;
            //output = Math.Round(output, 2);
            //output = string.Format()
            //Console.WriteLine($"${value1} + ${value2} + ${value3} + GST = ${output}");  <-- this is an old method, but it didn't format the results with single digit inputs so i tried a new method - left for posterity.

            string output = String.Format("{0:C2} + {1:C2} + {2:C2} + GST = {3:C2}",value1,value2,value3,total);
            Console.WriteLine(output);
        }
    }
}
