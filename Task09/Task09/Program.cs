﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task09
{
    class Program
    {
        static void Main(string[] args)
        {
            var year = 2016;
            var i = 0;


            Start:
            Console.WriteLine("What is the current year?");
            if (int.TryParse(Console.ReadLine(), out year))
            {
                Console.WriteLine($"I will check the next 20 years from {year} and advise if they are leap years.");

                for (i = 0; i < 20; i++)
                {
                    if (year % 4 == 0)
                    {
                        if (year % 100 != 0 || year % 400 == 0)
                        {
                            Console.WriteLine($"{year} is a leap year");
                        }
                    }
                    year++;

                }

            }

            else
            {
                Console.WriteLine("Please input a valid year");
                goto Start;

            }
                
           
       
        
        }
    }
}
